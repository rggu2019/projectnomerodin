# This Python file uses the following encoding: utf-8
import sys
import PyQt5
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QHBoxLayout


if __name__ == "__main__":
    app = QApplication([])
    widget = QWidget()
    layout = QHBoxLayout()
    button = QPushButton("Выход")
    button1 = QPushButton("О QT")
    layout.addWidget(button)
    layout.addWidget(button1)
    widget.setLayout(layout)
    button.clicked.connect(app.quit)
    button1.clicked.connect(app.aboutQt)
    widget.setWindowTitle("Топорин")
    widget.show()
    sys.exit(app.exec_())
