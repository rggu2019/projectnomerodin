import PyQt5
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QPushButton, QApplication, QLabel, QFileDialog, QVBoxLayout
from .ImageView import ImageView
from .image_functions import load_image, makeHalftone
from matplotlib import pyplot as plt

def main():
    app = QApplication.instance()
    if app is None:
        app = QApplication([])

    widget = QWidget()
    layoutH = QHBoxLayout()
    layoutV = QVBoxLayout()
    layoutV1 = QVBoxLayout()

    img1 = load_image("Леонардо.JPG")
    img2 = load_image("Леонардо.JPG")
    img3 = load_image("Разность.JPG")

    v = ImageView()
    v1 = ImageView()
    v2 = ImageView()
    v3 = ImageView()

    v.showImage(img1)
    v1.showImage(makeHalftone(img2))
    v2.showImage(img3)
    v3.showDifferenceImage(img1,img3)


    v.clicked.connect(lambda : print("yay"))

    lbl1 = QLabel("Изображение 1")
    lbl2 = QLabel("Изображение 2")
    lbl3 = QLabel("Полутон")
    lbl4 = QLabel("Разностное изображение")

    layoutV.addWidget(lbl1)
    layoutV.addWidget(v)
    layoutV1.addWidget(lbl3)
    layoutV1.addWidget(v1)
    layoutV.addWidget(lbl2)
    layoutV.addWidget(v2)
    layoutV1.addWidget(lbl4)
    layoutV1.addWidget(v3)
    layoutH.addLayout(layoutV)
    layoutH.addLayout(layoutV1)

    widget.setLayout(layoutH)
    widget.show()
    plt.show()
    app.exec_()
