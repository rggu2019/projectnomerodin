# This Python file uses the following encoding: utf-8
from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5.QtGui import QImage, QColor
from .image_functions import load_image

class ImageView(QtWidgets.QScrollArea):
    def __init__(self):
        QtWidgets.QScrollArea.__init__(self)
        self.label = QtWidgets.QLabel(self)
        self.label.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.setBackgroundRole(QtGui.QPalette.Mid)
        self.setWidget(self.label)
        self.setWidgetResizable(True)
        self.installEventFilter(self)

    def showImage(self, img:QtGui.QImage):
        self.label.setPixmap(QtGui.QPixmap().fromImage(img))

    def showDifferenceImage(self, img1:QtGui.QImage, img2:QtGui.QImage):

        newW = 0
        newH = 0

        if img1.width() > img2.width():
            newW = img2.width()
        else:
            newW = img1.width()

        if img1.height() > img2.height():
            newH = img2.height()
        else:
            newH = img1.height()

        for i in range(newW):
            for j in range(newH):

                color1 = img1.pixelColor(i,j)
                color2 = img2.pixelColor(i,j)

                R1 = color1.red()
                G1 = color1.green()
                B1 = color1.blue()

                R2 = color2.red()
                G2 = color2.green()
                B2 = color2.blue()

                newR = abs(R1 - R2)
                newG = abs(G1 - G2)
                newB = abs(B1 - B2)

                if(newR > 255):
                    newR = 255
                if(newG > 255):
                    newG = 255
                if(newB > 255):
                    newB = 255
                if(newR < 0):
                    newR = 0
                if(newG < 0):
                    newG = 0
                if(newB < 0):
                    newB = 0

                img1.setPixel(i, j, QColor(newR, newG, newB).rgb())

        self.label.setPixmap(QtGui.QPixmap().fromImage(img1))

    clicked = QtCore.pyqtSignal()

    def eventFilter(self, obj, event):
        if obj == self:
            if event.type() == QtCore.QEvent.MouseButtonRelease:
                if obj.rect().contains(event.pos()):
                    self.clicked.emit()
                    return True
        return False






