import os,sys
import PyQt5
from PyQt5.QtGui import QImage, QColor

def hist(img: QImage):
    ret = [[],[],[]]
    for m in ret:
        for i in range(256):
            m.append(0)
    for i in range(img.width()):
        for j in range(img.height()):
            color = img.pixelColor(i,j)
            ret[0][color.red()] += 1
            ret[1][color.green()] += 1
            ret[2][color.blue()] += 1
    return ret


def makeHalftone(img: QImage):
    for i in range(img.width()):
        for j in range(img.height()):
            color = img.pixelColor(i,j)
            R = color.red()
            G = color.green()
            B = color.blue()
            Gr = (R+G+B)/3
            if Gr > 255:
                Gr = 255
            if Gr < 0:
                Gr = 0
            img.setPixel(i,j, QColor(Gr,Gr,Gr).rgb())
    return img


def load_image(file_name):
    img = QImage()
    if not img.load(file_name):
        raise FileNotFoundError(file_name)
    return img
