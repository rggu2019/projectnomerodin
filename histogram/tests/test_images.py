import PyQt5
from image_editor import image_functions
from matplotlib import pyplot as plt

def test_all_white():
    img = image_functions.load_image("white.bmp")
    hist = image_functions.hist(img)
    assert len(hist) == 3
    for color_hist in hist:
        plt.plot(color_hist)
        assert len(color_hist) == 256
        assert sum(color_hist) == 10000
        assert color_hist[255] == 10000
        for el in color_hist[:-1]:
            assert el == 0

def test_all_black():
    img = image_functions.load_image("black.bmp")
    hist = image_functions.hist(img)
    assert len(hist) == 3
    for color_hist in hist:
        assert len(color_hist) == 256
        assert sum(color_hist) == 10000
        assert color_hist[0] == 10000
        for el in color_hist[1:]:
            assert el == 0

def test_all_red():
    img = image_functions.load_image("red.bmp")
    hist = image_functions.hist(img)
    assert len(hist) == 3
    for color_hist in hist:
        assert len(color_hist) == 256
        assert sum(color_hist) == 10000
        for el in color_hist[1:-1]:
            assert el == 0
        assert hist[0][0] == 0
        assert hist[0][255] == 10000
        assert hist[1][0] == 10000
        assert hist[1][255] == 0
        assert hist[2][0] == 10000
        assert hist[2][255] == 0

def test_all_green():
    img = image_functions.load_image("green.bmp")
    hist = image_functions.hist(img)
    assert len(hist) == 3
    for color_hist in hist:
        assert len(color_hist) == 256
        assert sum(color_hist) == 10000
        for el in color_hist[1:-1]:
            assert el == 0
        assert hist[0][0] == 10000
        assert hist[0][255] == 0
        assert hist[1][0] == 0
        assert hist[1][255] == 10000
        assert hist[2][0] == 10000
        assert hist[2][255] == 0

def test_all_blue():
    img = image_functions.load_image("blue.bmp")
    hist = image_functions.hist(img)
    assert len(hist) == 3
    for color_hist in hist:
        assert len(color_hist) == 256
        assert sum(color_hist) == 10000
        for el in color_hist[1:-1]:
            assert el == 0
        assert hist[0][0] == 10000
        assert hist[0][255] == 0
        assert hist[1][0] == 10000
        assert hist[1][255] == 0
        assert hist[2][0] == 0
        assert hist[2][255] == 10000
