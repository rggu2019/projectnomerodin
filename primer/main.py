# This Python file uses the following encoding: utf-8
import PyQt5
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QPushButton, QApplication

if __name__ == "__main__":
    app = QApplication([])
    widget = QWidget()
    layout = QHBoxLayout()
    button = QPushButton("Exit")
    button1 = QPushButton("About Qt")
    layout.addWidget(button)
    layout.addWidget(button1)
    widget.setLayout(layout)
    button.clicked.connect(app.quit)
    button1.clicked.connect(app.aboutQt)
    widget.show()
    app.exec_()
